import { BrowserWindow } from 'electron'

export interface IChannelBinding<T extends any[], U extends any[]> {
  bind(window: BrowserWindow): void
}
