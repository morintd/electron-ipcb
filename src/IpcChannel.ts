import { BrowserWindow, ipcMain, IpcMainEvent, ipcRenderer, IpcRendererEvent } from 'electron'

class IpcChannel<T extends any[], U extends any[]> {
  action: string

  constructor(action: string) {
    this.action = action
  }

  onMain(
    window: BrowserWindow,
    handler: (
      e: IpcMainEvent,
      send: (error: Error | undefined, ...result: U | []) => void,
      ...args: T
    ) => void
  ) {
    ipcMain.on(`${this.action}:request`, (event, ...args) => {
      handler(
        event,
        (error, ...result) => {
          if (error) this.throw(window, error)
          else this.send(window, ...(result as U))
        },
        ...(args as T)
      )
    })
  }

  onRenderer(
    cb: (e: IpcRendererEvent, ...args: U) => void,
    cbError: (e: IpcRendererEvent, error: Error) => void
  ) {
    ipcRenderer.on(`${this.action}:send`, cb as any)

    ipcRenderer.on(`${this.action}:error`, cbError)
  }

  removeListener(
    cb: (e: IpcRendererEvent, ...args: U) => void,
    cbError: (e: IpcRendererEvent, error: Error) => void
  ) {
    ipcRenderer.removeListener(`${this.action}:send`, cb as any)
    ipcRenderer.removeListener(`${this.action}:error`, cbError)
  }

  request(...args: T) {
    ipcRenderer.send(`${this.action}:request`, ...args)
  }

  send(window: Electron.BrowserWindow, ...result: U) {
    window.webContents.send(`${this.action}:send`, ...result)
  }

  throw(window: Electron.BrowserWindow, error: Error) {
    window.webContents.send(`${this.action}:error`, error)
  }
}

export default IpcChannel
