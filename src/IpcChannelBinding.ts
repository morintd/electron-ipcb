/* eslint-disable @typescript-eslint/no-explicit-any */
import { BrowserWindow, IpcMainEvent } from 'electron'

import IpcChannel from './IpcChannel'

class IpcChannelBinding<T extends any[], U extends any[]> {
  channel: IpcChannel<T, U>
  binding: (window: BrowserWindow, event: IpcMainEvent, ...args: T) => U | Promise<U>

  constructor(
    channel: IpcChannel<T, U>,
    binding: (window: BrowserWindow, event: IpcMainEvent, ...args: T) => U | Promise<U>
  ) {
    this.channel = channel
    this.binding = binding
  }

  bind(window: BrowserWindow) {
    this.channel.onMain(window, (event, send, ...args) => {
      try {
        Promise.resolve(this.binding(window, event, ...args))
          .then(result => {
            send(undefined, ...result)
          })
          .catch(err => {
            send(err)
          })
      } catch (e) {
        send(e)
      }
    })
  }
}

export default IpcChannelBinding
