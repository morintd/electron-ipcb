/* eslint-disable @typescript-eslint/no-explicit-any */
import { BrowserWindow, IpcMainEvent } from 'electron'
import { IChannelBinding } from './IChannelBinding'

import IpcChannel from './IpcChannel'

class IpcChannelBindingControlled<T extends any[], U extends any[]>
  implements IChannelBinding<T, U> {
  channel: IpcChannel<T, U>
  binding: (
    window: BrowserWindow,
    event: IpcMainEvent,
    send: (error: Error | undefined, ...args: U) => void,
    ...args: T
  ) => void

  constructor(
    channel: IpcChannel<T, U>,
    binding: (
      window: BrowserWindow,
      event: IpcMainEvent,
      send: (error: Error | undefined, ...args: U) => void,
      ...args: T
    ) => void
  ) {
    this.channel = channel
    this.binding = binding
  }

  bind(window: BrowserWindow) {
    this.channel.onMain(window, (event, send, ...args) => {
      this.binding(window, event, send, ...args)
    })
  }
}

export default IpcChannelBindingControlled
