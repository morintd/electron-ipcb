/* eslint-disable @typescript-eslint/no-explicit-any */
import { BrowserWindow, BrowserWindowConstructorOptions } from 'electron'

import { IChannelBinding } from './IChannelBinding'

class IpcWindow {
  window: BrowserWindow

  constructor(
    options: BrowserWindowConstructorOptions,
    channels?: Array<IChannelBinding<any, any>>
  ) {
    this.window = new BrowserWindow(options)
    if (channels) this.bindChannels(channels)
  }

  bindChannels(channels: Array<IChannelBinding<any, any>>) {
    channels.forEach(channel => {
      channel.bind(this.window)
    })
  }
}

export default IpcWindow
